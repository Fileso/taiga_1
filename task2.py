"""
Модуль task2 содержит функции для работы с бинарным поиском и генерации случайных чисел.
"""

import sys
import random

def binary_search(arr, x):
    """
    Функция для бинарного поиска в отсортированном массиве.

    :param arr: Отсортированный массив целых чисел.
    :param x: Значение, которое нужно найти.
    :return: Индекс элемента в массиве, если он найден, иначе -1.
    """
    low = 0
    high = len(arr) - 1
    mid = 0

    while low <= high:
        mid = (high + low) // 2

        if arr[mid] < x:
            low = mid + 1
        elif arr[mid] > x:
            high = mid - 1
        else:
            return mid

    return -1

def main():
    """
    Главная функция программы.
    """
    arr = sorted([random.randint(1, 1000) for _ in range(100)])
    print("Массив:", arr)

    if len(sys.argv) > 1:
        x = int(sys.argv[1])
    else:
        x = 0

    result = binary_search(arr, x)

    if result != -1:
        print("Индекс числа:", str(result))
    else:
        print("Не зарегало")

if __name__ == "__main__":
    main()
