#FROM python:3
#WORKDIR /app
#COPY task2.py ./
#ENTRYPOINT ["python3", "task2.py"]
#CMD $1

FROM python:3.9-slim
WORKDIR /app
COPY /FastAPI /app
RUN pip install -r ./requirements.txt
ENTRYPOINT ["python3", "task2_.py"]
